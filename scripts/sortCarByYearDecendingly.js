function sortCarByYearDescendingly(cars) {
  // Sangat dianjurkan untuk console.log semua hal hehe
  console.log(cars);
  // Clone array untuk menghindari side-effect
  // Apa itu side effect?
  const result = [...cars];

  // Tulis code-mu disini
  for(let i=0; i < result.length-1; i++){
    // for untuk mengulang pemindahan array yang nilainya lebih kecil
    for(let j=0; j < result.length-1; j++){
      // mengecek apakah tahun index ke j itu lebih besar dari index sesudahnya
      if(result[j].year <  result[j+1].year){
        // klo tahun index sesudahnya lebih kecil maka indek j ditaro dipenampungan
        penampungan = result[j];
           // posisi index j ditukar dengan sesudahnya karna tahunya lebih kecil
          result[j] = result[j+1];
          // contoh yg tdnya [1,2] menjadi [2,1]
          result[j+1] = penampungan;
        }
      }
    }   

  // setelah nilai dari result terurut berdasarkan tahun maka function nya mengembalikan nilai result
  return result;
}

