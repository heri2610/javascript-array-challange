function sortCarByYearAscendingly(cars) {
  // Sangat dianjurkan untuk console.log semua hal hehe
  console.log(cars);
  // Clone array untuk menghindari side-effect
  // Apa itu side effect?
  const result = [...cars];
  
  // Tulis code-mu disini
  // mengeluarkan isi array menggunakan for
  for(let i=0; i < result.length-1; i++){
    // for untuk mengulang pemindahan array yang nilainya lebih besar
    for(let j=0; j < result.length-1; j++){
      // mengecek apakah tahun index ke j itu lebih besar dari index sesudahnya
      if(result[j].year >  result[j+1].year){
        // klo tahun index sesudahnya lebih besar maka indek j ditaro dipenampungan
        penampungan = result[j];
        // posisi index j ditukar dengan sesudahnya karna tahunya lebih besar
          result[j] = result[j+1];
          // ditukar posisinya
          result[j+1] = penampungan;
          // contoh yg tdnya [2,1] menjadi [1,2]

        }
      }
    }   
// setelah nilai dari result terurut berdasarkan tahun maka function nya mengembalikan nilai result
  return result;
}